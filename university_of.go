package main

import (
	"flag"
	"git.snt.utwente.nl/silke/university_of/slogan"
	"log"
	"net/http"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	err := slogan.RenderIndex(w)
	if err != nil {
		log.Printf("Error while rendering index: %s", err)
	}
}

func main() {
	var addr string

	flag.StringVar(&addr, "addr", ":8080", "Listen address")
	flag.Parse()

	http.HandleFunc("/svg/", svgHandler)
	http.HandleFunc("/png/", pngHandler)
	http.HandleFunc("/", indexHandler)
	log.Printf("Listening on %s", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
