package slogan

import (
	"fmt"
	"html/template"
	"io"
	"strconv"
	"strings"
)

type Image int

const (
	ImageCount = 3
)

// Slogan represents a slogan configuration
type Slogan struct {
	Scheme *ColorScheme
	Image  Image
	Text   string
}

// stringToColorScheme converts a string to a color scheme.
func stringToColorScheme(scheme string) (s *ColorScheme, err error) {
	// Default value
	if scheme == "" {
		return ColorSchemes[0], nil
	}

	// Check if possibly integer
	if i, err := strconv.Atoi(scheme); err == nil {
		if i >= len(ColorSchemes) {
			return nil, fmt.Errorf("valid color schemes are 0-%v", len(ColorSchemes)-1)
		}
		return ColorSchemes[i], nil
	}

	// Treat as list
	return NewColorScheme(strings.Split(scheme, ",")...)
}

// NewSlogan returns a new slogan configuration
func NewSlogan(image int, scheme, text string) (*Slogan, error) {
	// Check image
	if image >= ImageCount {
		return nil, fmt.Errorf("invalid image %v, valid images are 0-%v", image, ImageCount-1)
	}

	// Check scheme
	sch, err := stringToColorScheme(scheme)
	if err != nil {
		return nil, fmt.Errorf("invalid color scheme %v: %s", scheme, err)
	}

	// Create and return slogan config
	s := &Slogan{
		Scheme: sch,
		Image:  Image(image),
		Text:   template.HTMLEscapeString(strings.ToUpper(text) + "."),
	}
	return s, nil
}

func (s *Slogan) ImageEnabled(image int) bool {
	return s.Image == Image(image)
}

func (s *Slogan) Render(wr io.Writer) error {
	return svgTemplate.Execute(wr, s)
}
