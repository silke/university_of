package slogan

import "io"

// Index contains all information to render an index
var index struct {
	Backgrounds, Schemes []int
	Colors               map[string]Color
}

// init initialises the main index struct
func init() {
	for i := 0; i < ImageCount; i++ {
		index.Backgrounds = append(index.Backgrounds, i)
	}
	for i := 0; i < len(ColorSchemes); i++ {
		index.Schemes = append(index.Schemes, i)
	}
	index.Colors = colors
}

// RenderIndex renders the index page
func RenderIndex(wr io.Writer) error {
	return indexTemplate.Execute(wr, index)
}
