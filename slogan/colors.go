package slogan

import (
	"fmt"
)

// Color definitions
const (
	Black     Color = "#000000"
	White     Color = "#ffffff"
	Salmon    Color = "#ebbda9"
	Pink      Color = "#cf0072"
	Purple    Color = "#4f2d7f"
	DarkBlue  Color = "#002c5f"
	Blue      Color = "#0094b3"
	LightBlue Color = "#63b1e5"
	DarkGreen Color = "#00675a"
	Green     Color = "#3f9c35"
	Olive     Color = "#887b1b"
	Yellow    Color = "#fed100"
	Orange    Color = "#ec7a08"
	Red       Color = "#c60c30"
	WineRed   Color = "#822433"
	WarmGrey  Color = "#513c40"
	ColdGrey  Color = "#616365"
	LightGrey Color = "#adafaf"
)

// colors contains the name to color mappings
var colors = map[string]Color{
	"black":     Black,
	"white":     White,
	"salmon":    Salmon,
	"pink":      Pink,
	"purple":    Purple,
	"darkblue":  DarkBlue,
	"blue":      Blue,
	"lightblue": LightBlue,
	"darkgreen": DarkGreen,
	"green":     Green,
	"olive":     Olive,
	"yellow":    Yellow,
	"orange":    Orange,
	"red":       Red,
	"winered":   WineRed,
	"warmgrey":  WarmGrey,
	"warmgray":  WarmGrey,
	"coldgrey":  ColdGrey,
	"coldgray":  ColdGrey,
	"lightgrey": LightGrey,
	"lightgray": LightGrey,
}

// Colorschemes contains the predefined color schemes
var ColorSchemes = []*ColorScheme{
	// Original combinations
	{White, Black, Pink},   // Mind-blowing open days
	{Black, White, Orange}, // Award-winning entrepreneurs
	{Black, White, Green},  // You and 40.000 other alumni
	{Orange, White, Black}, // Curious people

	// White, black, color
	{White, Black, Orange},
	{White, Black, Green},

	// Black, white, color
	{Black, White, Pink},

	// Color, black, white
	{Orange, Black, White},
	{Green, Black, White},
	{Pink, Black, White},

	// Color, white, black
	{Green, White, Black},
	{Pink, White, Black},
}

// Color represents a single color
type Color string

// NewColor returns the Color matching a name
func NewColor(name string) (Color, error) {
	c, ok := colors[name]
	if !ok {
		return "", fmt.Errorf("unknown color: %s", name)
	}
	return c, nil
}

// ColorScheme represents a color scheme of a slogan
type ColorScheme struct {
	Background, University, Slogan Color
}

// NewColorScheme creates a color scheme from several names.
// The order of names : slogan, background, university.
// If the length of colors is less than 3, the defaults (pink, white, black) will be substituted.
func NewColorScheme(colors ...string) (s *ColorScheme, err error) {
	s = &ColorScheme{White, Black, Pink}
	switch len(colors) {
	default:
		fallthrough
	case 3:
		s.University, err = NewColor(colors[2])
		if err != nil {
			return
		}
		fallthrough
	case 2:
		s.Background, err = NewColor(colors[1])
		if err != nil {
			return
		}
		fallthrough
	case 1:
		s.Slogan, err = NewColor(colors[0])
	}
	return
}
