package slogan

import (
	"github.com/gobuffalo/packr/v2"
	"text/template"
)

var svgTemplate, indexTemplate *template.Template
var svgTemplates = []string {
	"template.svg",
	"back0.svg",
	"back1.svg",
	"back2.svg",
}

func loadTemplate(t *template.Template, b *packr.Box, f string) {
	svg, err := b.FindString(f)
	if err != nil {
		panic(err)
	}

	t = template.Must(t.Parse(svg))
}

func init() {
	svgTemplate = template.New("svg")
	indexTemplate = template.New("index")

	box := packr.New("templates", "./templates")
	loadTemplate(indexTemplate,  box, "index.html")
	for _, f := range svgTemplates {
		loadTemplate(svgTemplate, box, f)
	}
}
