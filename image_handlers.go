package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"strconv"
	"time"

	"git.snt.utwente.nl/silke/university_of/slogan"
)

func getIntParam(r *http.Request, n string) (i int) {
	if s, ok := r.URL.Query()[n]; ok && len(s) > 0 {
		i, _ = strconv.Atoi(s[0])
	}
	return
}

func getStringParam(r *http.Request, n string) (s string) {
	if p, ok := r.URL.Query()[n]; ok && len(p) > 0 {
		return p[0]
	}
	return
}

func getSlogan(r *http.Request) (*slogan.Slogan, error) {
	// User input
	text := r.URL.Path[5:]

	// Image choice
	image := getIntParam(r, "image")

	// Color scheme
	scheme := getStringParam(r, "scheme")

	// Create slogan
	return slogan.NewSlogan(image, scheme, text)
}

func setHeaders(w http.ResponseWriter, contentType, filename string) {
	w.Header().Set("Content-Type", contentType)
	w.Header().Set("Cache-Control", "max-age:290304000, public")
	w.Header().Set("Last-Modified", time.Now().Format(http.TimeFormat))
	w.Header().Set("Content-Disposition", fmt.Sprintf(`inline; filename="%s"`, filename))
}

func svgHandler(w http.ResponseWriter, r *http.Request) {
	if checkCached(r) {
		w.WriteHeader(304)
		return
	}

	s, err := getSlogan(r)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "Error while creating slogan: %s", err)
		return
	}

	// Render image
	setHeaders(w, "image/svg+xml", s.Text + "svg")
	err = s.Render(w)
	if err != nil {
		log.Printf("Error while rendering: %s", err)
	}
}

func checkCached(r *http.Request) bool {
	_, ok := r.Header["If-Modified-Since"]
	return ok
}

func pngHandler(w http.ResponseWriter, r *http.Request) {
	if checkCached(r) {
		w.WriteHeader(304)
		return
	}

	width := getIntParam(r, "width")
	if width == 0 {
		width = 250
	}
	if width > 2560 {
		w.WriteHeader(400)
		fmt.Fprintf(w, "Width %v is not allowed.", width)
		return
	}

	s, err := getSlogan(r)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "Error while creating slogan: %s", err)
		return
	}

	// Set PNG header
	setHeaders(w, "image/png", s.Text + "png")

	// Create and start conversion command
	cmd, stdin, err := png2svg(w, width)
	if err != nil {
		log.Printf("Error starting command: %s", err)
	}

	// Render SVG to convert command
	err = s.Render(stdin)
	if err != nil {
		log.Printf("Error rendering template command: %s", err)
	}
	stdin.Close()

	// Wait for command to finish
	err = cmd.Wait()
	if err != nil {
		stderr, _ := cmd.StderrPipe()
		out, _ := ioutil.ReadAll(stderr)
		log.Printf("Error while running command: %v", out)
	}
}

func png2svg(out io.Writer, width int) (cmd *exec.Cmd, stdin io.WriteCloser, err error) {
	resize := fmt.Sprintf("%vx", width)
	density := fmt.Sprintf("%.0f", 96*float64(width)/250)

	cmd = exec.Command("convert",
		"-resize", resize,
		"-density", density,
		"svg:-", "png:-",
	)
	cmd.Stdout = out
	stdin, err = cmd.StdinPipe()
	if err != nil {
		return
	}
	return cmd, stdin, cmd.Start()
}
